﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
   public class InvalidArmorException: Exception
    {
        /// <summary>
        /// If the conditions to equipp a armor is false, then the exeption throws
        /// </summary>
        public InvalidArmorException()
        : base(String.Format("You cant equip this armor"))
        {

        }
    }
}
