﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int level { get; set; }
        public int BasePrimaryAttributes { get; set; }
        public int TotalPrimaryAttributes { get; set; }
        public int SecondaryAttributes { get; set; }
        public double DPS { get; set; }
        public bool EquippedWeapon { get; set; } = false;
        public bool EquippedArmor { get; set; } = false;

        public Dictionary<ItemSlots, ItemsAndEquipment> equipmentlist = new Dictionary<ItemSlots, ItemsAndEquipment>();

        public PrimaryAttributes primaryAttributes = new PrimaryAttributes();
        public Secondary_attributes secondary_Attributes = new Secondary_attributes();

        // Im am not proud of this method.... It has too much responsibility.. And i do repeat my self too much. lesson learned!!!
        /// <summary>       
        ///  Equipping armor to the character, and calculating the new stats. The armor objekt that is equipped, is created within this method.            
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl"></param>
        /// <param name="armorTypes"></param>
        /// <param name="primaryAttributes"></param>
        /// <param name="itemSlots"></param>
        /// <returns> A string that says the equippment went well</returns>
        /// <exception cref="InvalidArmorException"> When you try to equipp armor that you cant equipp, either if the requimrement level of the armor is too high or you try to equipp armor of wrong type </exception>
        public abstract string EquipArmor(string Name, int lvl, ArmorTypes armorTypes, PrimaryAttributes primaryAttributes, ItemSlots itemSlots);
        
        /// Im am not proud of this method.... It has too much responsibility.. And i do repeat my self too much. lesson learned!!!
        /// <summary>       
        /// Equipping weapon to the character, and calculating the new stats. The weapon objekt that is equipped, is created within this method
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl">Used to create the weapon objekt in the method</param>
        /// <param name="dmg">Used to create the weapon objekt in the method</param>
        /// <param name="attackspeed">Used to create the weapon objekt in the method</param>
        /// <param name="weaponType">Used to create the weapon objekt in the method</param>
        /// <param name="itemSlots">Used to create the armor objekt in the method</param>
        /// <returns> A string that says the equippment went well</returns>
        ///    /// <exception cref="InvalidWeaponException"> When you try to equipp a weapon that you cant equipp, either if the requimrement level of the weapon is too high, or you try to equipp it in wrong slot.
        public abstract string EquipWeapon(string Name, int lvl, int dmg, double attackspeed, WeaponType weaponType, ItemSlots itemSlots);
        
        /// <summary>
        /// Set the baseAttributes when character is lvl 1
         /// </summary>  
        public abstract void BaseAttributes();
  
        
        /// <summary>
        /// Displaying stats in console. Get the value by the lokal propps for the objekt.
        /// </summary> 
       public void Showstats()
        {
            Console.WriteLine($"Charactername: {Name}");
            Console.WriteLine($"Level: {level}");
            Console.WriteLine($"BasePrimaryAttributes:  Vitality: { primaryAttributes.Vitality}, Strenght: {primaryAttributes.Strength}, Dexterity: {primaryAttributes.Dexterity}, Intelligence: {primaryAttributes.Intelligence} ");
            Console.WriteLine($"TotalPrimaryAttributes: {TotalPrimaryAttributes}");
            Console.WriteLine($"SecondaryAttributes: Health: {secondary_Attributes.Health} Armor Rating: {secondary_Attributes.ArmorRating} Element Resistance: { secondary_Attributes.ElementalResistance}");
            Console.WriteLine($"Total dps: {DPS}");
        }

        /// <summary>
        /// levels the character up. Uppdating the attributes.
        /// </summary>
        /// <param name="lvl"> Takes in a number of what lvl you want to reach </param>
        /// <exception cref="ArgumentException"> If you try to gain 0 or less levels </exception>
        public abstract void LevelUp(int lvl);
             

        /// <summary>
        /// Calculating and updating the secondary attributes 
        /// </summary>
        public void CalculatecAttributes()
        {
            secondary_Attributes.Health = primaryAttributes.Vitality * 10;
            secondary_Attributes.ArmorRating = primaryAttributes.Strength + primaryAttributes.Dexterity;
            secondary_Attributes.ElementalResistance = primaryAttributes.Intelligence;
        }
    }
}
