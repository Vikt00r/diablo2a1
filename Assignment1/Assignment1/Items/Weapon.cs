﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class  Weapon : ItemsAndEquipment
    {

        public WeaponType WeaponType;
        public int basedamage { get; set; }
        public double attackspeed { get; set; }
        public double Dps { get; set; }

       

        public Weapon(string _name, int _lvl, int _basedmg, double _baseattackspeed, WeaponType _weapontype, ItemSlots _itemSlots)
        {
            ItemName = _name;
            ItemLevel = _lvl;
            basedamage = _basedmg;
            attackspeed = _baseattackspeed;
            WeaponType = _weapontype;
            itemSlot = _itemSlots;
            
        }

        public double Calculatedps()
        {
            Dps = basedamage * attackspeed;   
           return Dps;
        }

        public void ShowWeaponStats()
        {

            Console.WriteLine($"Character equipped: {ItemName} With lvl: {ItemLevel} Dmg: {basedamage} Attackspeed: {attackspeed}, Weapontype: {WeaponType} ");
        }

    }
}

