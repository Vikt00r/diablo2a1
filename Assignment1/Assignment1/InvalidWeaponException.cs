﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
  public  class InvalidWeaponException: Exception
    {


        /// <summary>
        /// If the conditions to equipp a weapon is false, then the exeption throws
        /// </summary>
        public InvalidWeaponException()
        : base(String.Format("You cant equip this weapon"))
        {

        }



    }
}
