﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
   public class Warrior : Character
    {
          
        public Warrior(string namn)
        {

            Name = namn;
            level = 1;
            DPS = 1;
            BaseAttributes();        // set baseattributes  
            CalculatecAttributes(); // calculate attributes
            
          
        }


        public override void BaseAttributes() 
        {
            primaryAttributes.Vitality = 10;
            primaryAttributes.Strength = 5;
            primaryAttributes.Dexterity = 2;
            primaryAttributes.Intelligence = 1;
        }

        

        public override void LevelUp(int Level)
        {
            if (Level > 0)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("---------------Warrior levled up---------------------------");
                Console.WriteLine("--------------------------------------------------------");
                level = Level;  
                primaryAttributes.Vitality += 5 * (Level - 1);
                primaryAttributes.Strength += 3 * (Level - 1);
                primaryAttributes.Dexterity += 2 * (Level - 1);
                primaryAttributes.Intelligence += 1 * (Level - 1);
                CalculatecAttributes();  // updating the sec attributes that is connected to the primary 
              
            }
            else throw new ArgumentException();
        }

        /// <summary>       
        /// Equipping weapon to the character, and calculating the new stats. The weapon objekt that is equipped, is created within this method
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl">Used to create the weapon objekt in the method</param>
        /// <param name="dmg">Used to create the weapon objekt in the method</param>
        /// <param name="attackspeed">Used to create the weapon objekt in the method</param>
        /// <param name="weaponType">Used to create the weapon objekt in the method</param>
        /// <param name="itemSlots">Used to create the armor objekt in the method</param>
        /// <returns> A string that says the equippment went well</returns>
        ///    /// <exception cref="InvalidWeaponException"> When you try to equipp a weapon that you cant equipp, either if the requimrement level of the weapon is too high, or you try to equipp it in wrong slot.
        public override string EquipWeapon(string Name, int lvl, int dmg, double attackspeed, WeaponType weaponType, ItemSlots itemSlots)
        {
            
            Weapon WeaponObj = new Weapon(Name, lvl, dmg, attackspeed, weaponType, itemSlots);

            if (level >= lvl &&  (weaponType.Equals(WeaponType.Axe) || weaponType.Equals(WeaponType.Sword) || weaponType.Equals(WeaponType.Hammer))) //checking if lvl & weapontype condition is true;
            {
                DPS = WeaponObj.Calculatedps() *  (1 + ((double)primaryAttributes.Strength / 100));    //Update the DPS                        
                equipmentlist.Add(ItemSlots.Weapon, WeaponObj);              
                return new string("New weapon equipped");
            }
            else throw new InvalidWeaponException();
        }
        /// <summary>       
        ///  Equipping armor to the character, and calculating the new stats. The armor objekt that is equipped, is created within this method.            
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl"></param>
        /// <param name="armorTypes"></param>
        /// <param name="primaryAttributes"></param>
        /// <param name="itemSlots"></param>
        /// <returns> A string that says the equippment went well</returns>
        /// <exception cref="InvalidArmorException"> When you try to equipp armor that you cant equipp, either if the requimrement level of the armor is too high or you try to equipp armor of wrong type </exception>
        public override string EquipArmor(string Name, int lvl, ArmorTypes armorTypes, PrimaryAttributes primaryAttributes, ItemSlots itemSlots)
        {
            
            Armor ArmorObj = new Armor(Name, lvl, armorTypes, primaryAttributes, itemSlots);

            
                if ((level >= lvl) &&  ArmorObj.itemSlot.Equals(ItemSlots.Body) || ArmorObj.itemSlot.Equals(ItemSlots.Legs) || ArmorObj.itemSlot.Equals(ItemSlots.Head) // checking if lvl ,Itemslots & weapontype condition is true;
                    && ArmorObj.ArmorTypes.Equals(ArmorTypes.Mail)
                    && ArmorObj.ArmorTypes.Equals(ArmorTypes.Plate)
                    )
                {

                    if (equipmentlist.ContainsKey(itemSlots))
                    {
                        equipmentlist[itemSlots] = ArmorObj;
                    }
                    else
                    {
                        equipmentlist.Add(itemSlots, ArmorObj);
                        
                    }

                    this.primaryAttributes.Dexterity += primaryAttributes.Dexterity;
                    this.primaryAttributes.Vitality += primaryAttributes.Vitality;
                    this.primaryAttributes.Intelligence += primaryAttributes.Intelligence;
                    this.primaryAttributes.Strength += primaryAttributes.Strength;
                    var weaponDps = ((Weapon)equipmentlist[ItemSlots.Weapon]).Calculatedps(); //equipp the weapon to Itemslot
                    DPS = weaponDps * ((1 + ((double)this.primaryAttributes.Strength / 100))); //update DPS

                    Console.WriteLine("-----------------------------");                                   
                    return new string ("New armor equipped");
                }
                else
                {
                    throw new InvalidArmorException();
                }
        }

    }
}
