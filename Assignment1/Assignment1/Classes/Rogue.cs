﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
   public class Rogue : Character
    {
    
   
        public int Total;


        public Rogue(string namn)
        {

            Name = namn;
            level = 1;
            DPS = 1;
            BaseAttributes();          
            CalculatecAttributes();
        }
        public override void BaseAttributes()
        {
            primaryAttributes.Vitality = 8;
            primaryAttributes.Strength = 2;
            primaryAttributes.Dexterity = 6;
            primaryAttributes.Intelligence = 1;
        }

        public override void LevelUp(int Level)
        {
            if (Level > 0)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("---------------Rogue levled up---------------------------");
                Console.WriteLine("--------------------------------------------------------");
                level = Level;
                primaryAttributes.Vitality += 3 * (Level - 1);
                primaryAttributes.Strength += 1 * (Level - 1);
                primaryAttributes.Dexterity += 4 * (Level - 1);
                primaryAttributes.Intelligence += 1 * (Level - 1);
                CalculatecAttributes(); // updating the sec attributes that is connected to the primary     
                
            }
            else throw new ArgumentException();
      }

        /// <summary>       
        /// Equipping weapon to the character, and calculating the new stats. The weapon objekt that is equipped, is created within this method
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl">Used to create the weapon objekt in the method</param>
        /// <param name="dmg">Used to create the weapon objekt in the method</param>
        /// <param name="attackspeed">Used to create the weapon objekt in the method</param>
        /// <param name="weaponType">Used to create the weapon objekt in the method</param>
        /// <param name="itemSlots">Used to create the armor objekt in the method</param>
        /// <returns> A string that says the equippment went well</returns>
        ///    /// <exception cref="InvalidWeaponException"> When you try to equipp a weapon that you cant equipp, either if the requimrement level of the weapon is too high, or you try to equipp it in wrong slot.
        public override string EquipWeapon(string Name, int lvl, int dmg, double attackspeed, WeaponType weaponType, ItemSlots itemSlots)
        {            
            Weapon WeaponObj = new Weapon(Name, lvl, dmg, attackspeed, weaponType, itemSlots);

            if (level >= lvl && (weaponType.Equals(WeaponType.Dagger) || weaponType.Equals(WeaponType.Sword))) //checking if lvl & weapontype condition is true
            {
                DPS = WeaponObj.Calculatedps() * (1 + primaryAttributes.Dexterity / 100);               
                equipmentlist.Add(ItemSlots.Weapon, WeaponObj);         
                return new string("New weapon equipped");
            }
            else throw new InvalidWeaponException();
        }

        /// <summary>       
        ///  Equipping armor to the character, and calculating the new stats. The armor objekt that is equipped, is created within this method.            
        /// </summary>
        /// <param name="Name">Used to create the armor objekt in the method</param>
        /// <param name="lvl">Used to create the armor objekt in the method</param>
        /// <param name="armorTypes">Used to create the armor objekt in the method</param>
        /// <param name="primaryAttributes">Used to create the armor objekt in the method</param>
        /// <param name="itemSlots">Used to create the armor objekt in the method</param>
        /// <returns> A string that says the equippment went well</returns>
        /// <exception cref="InvalidArmorException"> When you try to equipp armor that you cant equipp, either if the requimrement level of the armor is too high or you try to equipp armor of wrong type </exception>
        public override string EquipArmor(string Name, int lvl, ArmorTypes armorTypes, PrimaryAttributes primaryAttributes, ItemSlots itemSlots) 
        {          
            Armor a = new Armor(Name, lvl, armorTypes, primaryAttributes, itemSlots);

            if (level >= lvl && a.itemSlot.Equals(ItemSlots.Body) || a.itemSlot.Equals(ItemSlots.Legs) || a.itemSlot.Equals(ItemSlots.Head)// checking if lvl, itemslot and amortype condition is true;
                && a.ArmorTypes.Equals(ArmorTypes.Leather)
                && a.ArmorTypes.Equals(ArmorTypes.Mail)
                )
            {
                if (equipmentlist.ContainsKey(itemSlots))
                {
                    equipmentlist[itemSlots] = a; // replace the old armor
                }
                else
                {
                    equipmentlist.Add(itemSlots, a);
                }
                this.primaryAttributes.Dexterity +=primaryAttributes.Dexterity;
                this.primaryAttributes.Vitality += primaryAttributes.Vitality;
                this.primaryAttributes.Intelligence += primaryAttributes.Intelligence;
                this.primaryAttributes.Strength += primaryAttributes.Strength;
                var weaponDps = ((Weapon)equipmentlist[ItemSlots.Weapon]).Calculatedps(); //equipp the armor to Itemslot
                DPS = weaponDps * ((1 + ((double)this.primaryAttributes.Dexterity / 100))); // update the DPS

                Console.WriteLine("-----------------------------");
                Console.WriteLine("You have equip armor : " + Name);              
                
                return new string("New armor equipped");

            }
            else
            {
                throw new InvalidWeaponException();
            }

        }


    }
}
