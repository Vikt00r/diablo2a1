﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
   public class Mage : Character
    {
      

        public Mage(string namn)
        {
            Name = namn;
            level = 1;
            DPS = 1;
            BaseAttributes();          
            CalculatecAttributes();        
        }
        public override void BaseAttributes()
        {
            primaryAttributes.Vitality = 5;
            primaryAttributes.Strength = 1;
            primaryAttributes.Dexterity = 1;
            primaryAttributes.Intelligence = 8;
        }

        public override void LevelUp(int Level)
        {
            if (Level > 0)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("---------------Mage levled up---------------------------");
                Console.WriteLine("--------------------------------------------------------");
                level = Level;
                primaryAttributes.Vitality += 3 * (Level - 1);
                primaryAttributes.Strength += 1 * (Level - 1);
                primaryAttributes.Dexterity += 1 * (Level - 1);
                primaryAttributes.Intelligence += 5 * (Level - 1);
                CalculatecAttributes(); // updating the sec attributes that is connected to the primary                
            }
            else throw new ArgumentException();

    }
        /// <summary>       
        /// Equipping weapon to the character, and calculating the new stats. The weapon objekt that is equipped, is created within this method
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl">Used to create the weapon objekt in the method</param>
        /// <param name="dmg">Used to create the weapon objekt in the method</param>
        /// <param name="attackspeed">Used to create the weapon objekt in the method</param>
        /// <param name="weaponType">Used to create the weapon objekt in the method</param>
        /// <param name="itemSlots">Used to create the armor objekt in the method</param>
        /// <returns> A string that says the equippment went well</returns>
        ///    /// <exception cref="InvalidWeaponException"> When you try to equipp a weapon that you cant equipp, either if the requimrement level of the weapon is too high, or you try to equipp it in wrong slot.
        public override string EquipWeapon(string Name, int lvl, int dmg, double attackspeed, WeaponType weaponType, ItemSlots itemSlots)
        {
           
            Weapon WeaponObj = new Weapon(Name, lvl, dmg, attackspeed, weaponType, itemSlots);

            if (level >= lvl && (weaponType.Equals(WeaponType.Wand) || weaponType.Equals(WeaponType.Staff))) //checking if lvl & weapontype condition is true
            {
                DPS = WeaponObj.Calculatedps() * (1 + primaryAttributes.Intelligence/ 100)  ;              
                equipmentlist.Add(ItemSlots.Weapon, WeaponObj);        
                return new string("New weapon equipped");
            }
            else throw new InvalidWeaponException();
        }

        /// <summary>       
        ///  Equipping armor to the character, and calculating the new stats. The armor objekt that is equipped, is created within this method.            
        /// </summary>
        /// <param name="Name">Used to create the weapon objekt in the method</param>
        /// <param name="lvl"></param>
        /// <param name="armorTypes"></param>
        /// <param name="primaryAttributes"></param>
        /// <param name="itemSlots"></param>
        /// <returns> A string that says the equippment went well</returns>
        /// <exception cref="InvalidArmorException"> When you try to equipp armor that you cant equipp, either if the requimrement level of the armor is too high or you try to equipp armor of wrong type </exception>
        public override string EquipArmor(string Name, int lvl, ArmorTypes armorTypes, PrimaryAttributes _primaryAttributes, ItemSlots itemSlots)
        {       
           Armor a = new Armor(Name, lvl, armorTypes, _primaryAttributes, itemSlots);

            if(level >= lvl && a.itemSlot.Equals(ItemSlots.Body) || a.itemSlot.Equals(ItemSlots.Legs) || a.itemSlot.Equals(ItemSlots.Head) && a.ArmorTypes.Equals(ArmorTypes.Cloth)) //checking if lvl & weapontype condition is true
            {
                if (equipmentlist.ContainsKey(itemSlots))
                {
                    equipmentlist[itemSlots] = a; //replace the old armor 
                }
                else
                {
                    equipmentlist.Add(itemSlots, a);

                } 
                
            primaryAttributes.Dexterity += _primaryAttributes.Dexterity; 
            primaryAttributes.Vitality += _primaryAttributes.Vitality;
            primaryAttributes.Intelligence += _primaryAttributes.Intelligence;
            primaryAttributes.Strength += _primaryAttributes.Strength;
            var weaponDps = ((Weapon)equipmentlist[ItemSlots.Weapon]).Calculatedps();//add the weapon to Itemslot
                DPS = weaponDps * ((1 + ((double)this.primaryAttributes.Intelligence / 100))); //update the DPS

            Console.WriteLine("-----------------------------");
                Console.WriteLine("You have equip armor : " + Name);            
                
                return new string("New armor equipped");

            }
            else
            {
                throw new InvalidWeaponException();
            }

        }
          
        }


    }

