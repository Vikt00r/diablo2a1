﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assignment1;

namespace Assignment1tests
{
    public class Charactertests

    {
        [Fact]
        public void CreateCharacter_Charcter_lvl1_when_Created()
        {
            //arrange
            Mage mage1 = new Mage("m1");
            int lvlexpected = 1;

            //Act
            int actual = mage1.level;
            //Assert       
            Assert.Equal(lvlexpected, actual);

        }

        [Fact]
        public static void Character_Character_gain_one_lvl()
        {

            //arrange
            Mage mage1 = new Mage("Linn");
            int lvlexpected = 2;

            //act
            mage1.LevelUp(lvlexpected);

            //assert

            Assert.Equal(lvlexpected, mage1.level);

        }

        [Fact]
        public static void Character_Mage__EachCharacterClassIsCreatedWiththeProperDefaultAttributes()
        {
            //arrange
            Mage m1 = new Mage("Linn");

            int excvitality = 5;
            int excStrength = 1;
            int excDexterity = 1;
            int excIntelligence = 8;

            // act
            int actualVitality = m1.primaryAttributes.Vitality;
            int actualStenght = m1.primaryAttributes.Strength;
            int actualDexterity = m1.primaryAttributes.Dexterity;
            int actualIntelligence = m1.primaryAttributes.Intelligence;

            //assert
            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }
        [Fact]
        public static void Character_Ranger__EachCharacterClassIsCreatedWiththeProperDefaultAttributes()
        {
            //arrange
            Ranger r1 = new Ranger("Linn");

            int excvitality = 8;
            int excStrength = 1;
            int excDexterity = 7;
            int excIntelligence = 1;

            // act
            int actualVitality = r1.primaryAttributes.Vitality;
            int actualStenght = r1.primaryAttributes.Strength;
            int actualDexterity = r1.primaryAttributes.Dexterity;
            int actualIntelligence = r1.primaryAttributes.Intelligence;

            //assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }

        [Fact]
        public static void Character_Rouge__EachCharacterClassIsCreatedWiththeProperDefaultAttributes()
        {
            //arrange
            Rogue r1 = new Rogue("Linn");

            int excvitality = 8;
            int excStrength = 2;
            int excDexterity = 6;
            int excIntelligence = 1;

            // act
            int actualVitality = r1.primaryAttributes.Vitality;
            int actualStenght = r1.primaryAttributes.Strength;
            int actualDexterity = r1.primaryAttributes.Dexterity;
            int actualIntelligence = r1.primaryAttributes.Intelligence;

            //assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }
        [Fact]
        public static void Character_Warrior_EachCharacterClassIsCreatedWiththeProperDefaultAttributes()
        {
            //arrange
            Warrior w1 = new Warrior("Linn");

            int excvitality = 10;
            int excStrength = 5;
            int excDexterity = 2;
            int excIntelligence = 1;

            // act
            int actualVitality = w1.primaryAttributes.Vitality;
            int actualStenght = w1.primaryAttributes.Strength;
            int actualDexterity = w1.primaryAttributes.Dexterity;
            int actualIntelligence = w1.primaryAttributes.Intelligence;

            //assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }


        [Fact]
        public static void Character_Warrior_EachCharacterClassHasTheirAttributesIncreasedWhenLevelingUp()
        {
            // gör för alla karaktärer

            //arrange
            Warrior w1 = new Warrior("Linn");

            w1.LevelUp(2);

            int excvitality = 15;
            int excStrength = 8;
            int excDexterity = 4;
            int excIntelligence = 2;

            //Act
            int actualVitality = w1.primaryAttributes.Vitality;
            int actualStenght = w1.primaryAttributes.Strength;
            int actualDexterity = w1.primaryAttributes.Dexterity;
            int actualIntelligence = w1.primaryAttributes.Intelligence;

            //Assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }

        [Fact]
        public static void Character_Mage_EachCharacterClassHasTheirAttributesIncreasedWhenLevelingUp()
        {

            //arrange
            Mage m1 = new Mage("Linn");

            m1.LevelUp(2);

            int excvitality = 8;
            int excStrength = 2;
            int excDexterity = 2;
            int excIntelligence = 13;

            //Act
            int actualVitality = m1.primaryAttributes.Vitality;
            int actualStenght = m1.primaryAttributes.Strength;
            int actualDexterity = m1.primaryAttributes.Dexterity;
            int actualIntelligence = m1.primaryAttributes.Intelligence;

            //Assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }

        [Fact]
        public static void Character_Ranger_EachCharacterClassHasTheirAttributesIncreasedWhenLevelingUp()
        {
            // gör för alla karaktärer

            //arrange
            Ranger r1 = new Ranger("Linn");

            r1.LevelUp(2);

            int excvitality = 10;
            int excStrength = 2;
            int excDexterity = 12;
            int excIntelligence = 2;

            //Act
            int actualVitality = r1.primaryAttributes.Vitality;
            int actualStenght = r1.primaryAttributes.Strength;
            int actualDexterity = r1.primaryAttributes.Dexterity;
            int actualIntelligence = r1.primaryAttributes.Intelligence;

            //Assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }



        [Fact]
        public static void Character_Rougue_EachCharacterClassHasTheirAttributesIncreasedWhenLevelingUp()
        {
            //arrange
            Rogue r1 = new Rogue("Linn");

            r1.LevelUp(2);

            int excvitality = 11;
            int excStrength = 3;
            int excDexterity = 10;
            int excIntelligence = 2;

            //Act
            int actualVitality = r1.primaryAttributes.Vitality;
            int actualStenght = r1.primaryAttributes.Strength;
            int actualDexterity = r1.primaryAttributes.Dexterity;
            int actualIntelligence = r1.primaryAttributes.Intelligence;

            //Assert

            Assert.Equal(excvitality, actualVitality);
            Assert.Equal(excStrength, actualStenght);
            Assert.Equal(excDexterity, actualDexterity);
            Assert.Equal(excIntelligence, actualIntelligence);

        }




        [Fact]
        public static void Character_SecondaryStatsAreCalculatedFromLevelledUpCharacter()
        {
            //arrange
            Warrior w1 = new Warrior("Ralf");
            w1.LevelUp(2);

            int excHealth = 150;
            int excArmorRating = 12;
            int excElementalResistance = 2;

            //act

            int actualHealth = w1.secondary_Attributes.Health;
            int actualArmorRating = w1.secondary_Attributes.ArmorRating;
            int actualElementalResistance = w1.secondary_Attributes.ElementalResistance;


            //Assert

            Assert.Equal(excHealth, actualHealth);
            Assert.Equal(excArmorRating, actualArmorRating);
            Assert.Equal(excElementalResistance, actualElementalResistance);
        }


        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public static void IfYouTryToGain0OrLessLevelsAnArgumentExceptionShouldBeThrown(int InlineData)
        {
            //arrange                             
            Mage mage = new Mage("Linn");

            //Assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(InlineData));
        }
    }




}

