﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assignment1;

namespace Assignment1tests
{
    public class ItemAndEquipmentTests
    {

        [Fact]
        public void ItemAndEquipment_IfACharacterTriesToEquipAHighLevelWeaponInvalidWeaponExceptionShouldBethrown()
        {
            //arrange
            Warrior w1 = new Warrior("Ralf");

            //Assert       
            Assert.Throws<InvalidWeaponException>(() => w1.EquipWeapon("Common axe", 2, 7, 1.1, WeaponType.Axe, ItemSlots.Weapon));

        }

        [Fact]
        public void ItemAndEquipment_IfACharacterTriesToEquipAHighLevelArmorPieceInvalidArmorExceptionShouldBeThrown()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");

                //Assert       
                Assert.Throws<InvalidArmorException>(() => w1.EquipArmor("Common plate body armor", 2, ArmorTypes.Mail, new PrimaryAttributes() { Vitality = 1, Strength = 1, Dexterity = 1, Intelligence = 1 }, ItemSlots.Body));
            }
        }

        [Fact]
        public void ItemAndEquipment_IfACharacterTriesToEquipTheWrongWeaponTypeInvalidWeaponExceptionShouldBeThrown()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");

                //Assert       
                Assert.Throws<InvalidWeaponException>(() => w1.EquipWeapon("shitadel", 2, 5, 5, WeaponType.Wand, ItemSlots.Weapon));
            }
        }
        [Fact]
        public void ItemAndEquipment_IfACharacterEquipsAValidWeaponASuccessMessageShouldBeReturned()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");

                String expectedMessege = "New weapon equipped";
                //Assert       
                Assert.Equal(expectedMessege, w1.EquipWeapon("common axe", 1, 7, 1.1, WeaponType.Sword, ItemSlots.Weapon));
           }
        }

        [Fact]
        public void ItemAndEquipment_IfACharacterEquipsAValidArmorASuccessMessageShouldBeReturned()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf"); 
                String expectedMessege = "New armor equipped";

                //act
                w1.EquipWeapon("common axe", 1, 7, 1.1, WeaponType.Sword, ItemSlots.Weapon);             
                
                //Assert       
                Assert.Equal(expectedMessege, w1.EquipArmor("Common plate body armor", 1, ArmorTypes.Mail, new PrimaryAttributes() { Vitality = 1, Strength = 1, Dexterity = 1, Intelligence = 1 }, ItemSlots.Body));
            }
        }

        [Fact]
        public void ItemAndEquipment_CalculateDPSIfNoWeaponIsEquipped()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");

                double expecteddps = 1 * (1 + (5 / 100));

                //act 
                Double ActualDPS = w1.DPS;
                
                //Assert       

                Assert.Equal(expecteddps, ActualDPS);

            }
        }

       [Fact]
        public void ItemAndEquipment_voidCalculateDPSwithValidWeaponIsEquipped()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");           
                w1.EquipWeapon("common axe", 1, 7, 1.1, WeaponType.Axe, ItemSlots.Weapon);
                double expecteddps = (7 * 1.1) * (1 + (5.0 / 100));

                //act
                double actualDPS = w1.DPS;

                //Assert       

                Assert.Equal(expecteddps, actualDPS);
            }
        }

        [Fact]
        public void ItemAndEquipment_voidCalculateDPSwithValidWeaponAndArmorEquipped()
        {
            {
                //arrange
                Warrior w1 = new Warrior("Ralf");
                               
                w1.EquipWeapon("common axe", 1, 7, 1.1, WeaponType.Axe, ItemSlots.Weapon);
                w1.EquipArmor("Common plate body armor", 1, ArmorTypes.Plate, new PrimaryAttributes() { Vitality = 2, Strength = 1 }, ItemSlots.Body);
       
                double expecteddps = (7 * 1.1) * (1 + ((5 + 1) / 100.0));
                //Assert       

                //ACT
                double actualDPS = w1.DPS;

                Assert.Equal(expecteddps, actualDPS);


            }
        }

    }
}
